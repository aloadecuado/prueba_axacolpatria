//
//  PopUpUbicarViewController.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/28/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit
import MapKit
//showDetail
class PopUpUbicarViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var sbEscoger: UISegmentedControl!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var lbUbicacion: UILabel!
    
    var AcptarUbicacion:((_ Ubicaicon:String)->Void)?
    
    
    var location = CLLocationCoordinate2D(latitude: 0.0,longitude: 0.0)
    var CenterLocation = CLLocation(latitude: 0.0,longitude: 0.0)
    
    enum UbicacionPoin {
        case Direccion
        case Ciudad
        case Pais
    }
    
    var UbicacionSelccionada =  UbicacionPoin.Direccion
    
    var sUbicacion = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        irLocation(location: location)
        
        map.delegate = self
        
        CenterLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        TraerEncoder(center: CenterLocation)
    }
    func irLocation(location:CLLocationCoordinate2D)
    {
        
        let center = location
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.map.setRegion(region, animated: true)
        
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = mapView.centerCoordinate
        CenterLocation = CLLocation(latitude: center.latitude, longitude: center.longitude)
        TraerEncoder(center: CenterLocation)
        
        
        
    }
    
    @IBAction func triAceptar(_ sender: UIButton) {
        
        
        self.dismiss(animated: true, completion: {
            self.AcptarUbicacion!(self.sUbicacion)
            
            
        })
    }
    
    func TraerEncoder(center: CLLocation)
    {
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(center, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            //var placeMark: CLPlacemark!
            //placeMark = placemarks?[0]
            if let placesMarkets = placemarks{
                for placeMark in placesMarkets
                {
                    
                    // Address dictionary
                    print(placeMark)
                    
                    var sUbicacion = ""
                    switch self.UbicacionSelccionada {
                    case .Direccion:
                        // Location name
                        if let locationName = placeMark.thoroughfare {
                            sUbicacion = locationName as String
                            print(locationName)
                            break
                        }
                    case .Ciudad:
                        // City
                        if let locationName = placeMark.thoroughfare {
                            sUbicacion = locationName as String
                            print(locationName)
                            break
                        }
                    case .Pais:
                        if let locationName = placeMark.thoroughfare {
                            sUbicacion = locationName as String
                            print(locationName)
                            break
                        }
                    }
                    
                    var completeAddress = ""
                    if let locationName = placeMark.subThoroughfare {
                        completeAddress = locationName
                    }
                    
                    
                    // Country
                    self.sUbicacion = sUbicacion + " # " + completeAddress
                    self.lbUbicacion.text = sUbicacion + " # " + completeAddress
                    
                }
            }
            
        })
    }
    
    
    
    @IBAction func triChangeSegment(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            UbicacionSelccionada = UbicacionPoin.Pais
        case 1:
            UbicacionSelccionada = UbicacionPoin.Ciudad
        case 2:
            UbicacionSelccionada = UbicacionPoin.Direccion
        default:
            UbicacionSelccionada = UbicacionPoin.Ciudad
        }
        
        TraerEncoder(center: CenterLocation)
        
        
    }



}
