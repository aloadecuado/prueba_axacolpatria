//
//  OldmutualTextField.swift
//  co.facilpay.facilpayiostemporal
//
//  Created by Pedro Alonso Daza B on 26/09/18.
//  Copyright © 2018 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit
import Material
import DatePickerDialog

enum TipoDeCampo {
    case Lista
    case Texto
    case TextoAlfabetico
    case TextoNumerico
    case TextoAlfabeticoNumerico
    case TextoCifraPesos
    case Fecha
    case FechaNext
    case Lugar
    case LugarCiudad
    case LugarDepartamento
    case LugarPais
    case TextoEmail
    case SelectedPicker
}

@objc(OldmutualTextFieldDelegate)
protocol IndetTextFieldDelegate{
    
    @objc
    optional func IndetTextField(textField: UITextField, CambioFecha FechaDate: NSDate)
    
}
class IndetTextField: TextField, TextFieldDelegate
{

    var Tipo = TipoDeCampo.Texto
    var Controller:UIViewController?
    var Rango = 140
    var isRequired = false
    var Oldmutualdelegate: IndetTextFieldDelegate?
    var MaxCurrency = "5000000000000000"
    var MinCurrency = "0"
    //Lugar
    
    var MinDate = ""
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
      
        //Plet textff = vv as! TextField
        self.placeholderActiveColor = ColorPing
        //self.placeholderNormalColor = ColorPing
        self.dividerActiveColor = UIColor.lightGray
        self.detailColor = ColorErrorRed
        
        /*let font = UIFont(name: FontCentury, size: 18.0)!
        self.font =  font
        
        popDatePicker = PopDatePicker(forTextField: self)*/
        
        
    }

    
    
    func setTexto(Rango:Int)
    {
        Tipo = .Texto
        self.Rango = Rango
    }
    
    func setTextoAlfabetico(Rango:Int)
    {
        Tipo = .TextoAlfabetico
        self.Rango = Rango
    }
    
    func setTextoNumerico(Rango:Int)
    {
        Tipo = .TextoNumerico
        self.Rango = Rango
        self.keyboardType = .numberPad
    }
    
    func setTextoAlfabetoNumerico(Rango:Int)
    {
        Tipo = .TextoAlfabeticoNumerico
        self.Rango = Rango
    }
    
    func setTextoCifra(Rango:Int,Max:String = "500000000000" , Min:String="0")
    {
        Tipo = .TextoCifraPesos
        self.MaxCurrency = Max
        self.MinCurrency = Min
        self.Rango = Rango
        self.keyboardType = .numberPad
    }
    
    func setTextoFecha(Controller:UIViewController,MinDate:String="")
    {
        self.Tipo = .Fecha
        self.MinDate = MinDate
        self.Controller = Controller
    }
    func setDelegateSelf()
    {
        self.delegate = self
    }
    func setTextoLugarCiudad(Controller:UIViewController)
    {
        self.Tipo = .LugarCiudad
        self.Controller = Controller
    }
    
    func setTextoLugarDepartamento(Controller:UIViewController)
    {
        self.Tipo = .LugarDepartamento
        self.Controller = Controller
    }
    
    func setTextoLugarpais(Controller:UIViewController)
    {
        self.Tipo = .LugarPais
        self.Controller = Controller
    }
    
    func setTextoEmail()
    {
        self.Tipo = .TextoEmail
        
        self.keyboardType = .emailAddress
    }

    
    // el color de la sangre mancha mis manos mientras aplasto su craneo, ya hace tiempo que dejo de moverse de gritar de pedir piedad, es ctartico, ah cada nuevo golpe siento mas ganas de golpear y ver su rostro desaserse bajo mis puños, me brinda una sensacion liberadora, pronto todo habra terminado, pronto viviremos en un mundo puro
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if Tipo == .TextoAlfabetico
        {
            if (textField.text?.count)! > Rango
            {
                return false
            }
            return ValidateStringContent(string: string, Exist: VALIDATIONALFABETO)
        }
            
        else if Tipo == .TextoNumerico
        {
            return ValidationKeyNumbers(s: textField.text! + string, range: Rango)
        }
        else
        {
            return true;
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if Tipo == .Fecha{

            textField.resignFirstResponder()
            Controller!.view.endEditing(true)
            DatePickerDialog().show("DatePicker", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", datePickerMode: .date) {
                (date) -> Void in
                if let dt = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy"
                    self.text = formatter.string(from: dt)
                }
            }
            
        }
    }
    func textFieldDidEndEditing(_ textField: TextField) {
        textField.resignFirstResponder()
        Controller!.view.endEditing(true)
        if Tipo == .TextoEmail
        {
            let tf = textField as! TextField
            tf.detail = ""
            if !ValidationEmail(testStr: textField.text!)
            {
                tf.detail = ErrosEmail
            }
        }
        if Tipo == .TextoCifraPesos
        {
            let sNumerop = "\(textField.text!)"
            let sNum = CambioFormatoLetras(SCifra: sNumerop, MaxLong: MaxCurrency, MinLong: MinCurrency)
            textField.text = sNum
        }
        
        
    }
    func textField(textField: TextField, didChange text: String?) {
        if Tipo == .TextoCifraPesos
        {
            let sNumerop = "\(textField.text!)"
            let sNum = CambioFormatoLetras(SCifra: sNumerop, MaxLong: MaxCurrency, MinLong: "0")
            textField.text = sNum
        }
    }
}


