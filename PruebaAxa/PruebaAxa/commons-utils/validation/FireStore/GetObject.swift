//
//  GetObject.swift
//  Prueba_Rappi
//
//  Created by Propio Data on 5/12/19.
//  Copyright © 2019 VeriTran. All rights reserved.
//

import Foundation
import UIKit
import Firebase

func getObjectsWithStringEqualsString(collection:String, atribute:String, eQuals:String, ok:@escaping ((NSArray, NSArray)->Void), noExist:@escaping (()->Void),error:@escaping ((String)->Void)) {
    NSLog("Coleccion: %@  Atributo: %@ Objeto: %@", "\(collection)", "\(atribute)", "\(eQuals)")
    let db = Firestore.firestore()
    db.collection(collection).whereField(atribute, isEqualTo: eQuals).getDocuments() { (querySnapshot, err) in
        if let err = err {
            NSLog("Error : %@", "\(err.localizedDescription)")
            print("Error : %@", "\(err.localizedDescription)")
            error("\(err.localizedDescription)")
        } else {
            if(querySnapshot!.documents.count >= 1){
                print(querySnapshot!.documents)
                var dics = [NSDictionary]()
                var keys = [String]()
                for document in querySnapshot!.documents {
                    NSLog("id : \(document.documentID) Data: \(document.data())")
                    print("id : \(document.documentID) Data: \(document.data())")
                    dics.append(document.data() as NSDictionary)
                    keys.append(document.documentID)
                }
                ok(dics as NSArray, keys as NSArray)
            }else{
                noExist()
            }
            
        }
    }
}
