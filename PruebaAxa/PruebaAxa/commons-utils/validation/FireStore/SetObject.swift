//
//  SetObject.swift
//  Prueba_Rappi
//
//  Created by Propio Data on 5/12/19.
//  Copyright © 2019 VeriTran. All rights reserved.
//

import Foundation
import UIKit
import Firebase

func setObject(collection:String, dic:NSDictionary, ok:@escaping ((NSDictionary,String) -> Void), error:@escaping ((String) -> Void)) {
    NSLog("Coleccion: %@  Diccionario: %@", "\(collection)", "\(dic)")
    let db = Firestore.firestore()
    
    var ref: DocumentReference? = nil
    ref = db.collection(collection).addDocument(data: dic as! [String : Any]) { err in
        if let err = err {
            NSLog("Error : %@", "\(err.localizedDescription)")
            print("Error : %@", "\(err.localizedDescription)")
            error("\(err.localizedDescription)")
        } else {
            NSLog("id : \(ref!.documentID) Data: \(dic)")
            print("id : \(ref!.documentID) Data: \(dic)")
            ok(dic, ref!.documentID)
        }
    }
}



