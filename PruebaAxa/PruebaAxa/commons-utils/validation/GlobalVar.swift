//
//  GlobalVar.swift
//  PruebaGrupoCMC
//
//  Created by Propio Data on 4/26/19.
//  Copyright © 2019 Propio Data. All rights reserved.
//

import Foundation
import UIKit
import Material

let leng = "Esp"
let ErrorEmpty = (leng == "Esp") ? "Por favor Diligenciar este campo." : ""
let ErrosEmail = (leng == "Esp") ? "No es un email valido." : ""
let alMessageerrorBeforeDate = (leng == "Esp") ? "La fecha de expiración debe ser 3 meses antes o despues de la fecha de la compra del seguro." : ""
let alMessageerrorNoPhotto = (leng == "Esp") ? "por favor seleccione una foto" : ""
let alMessageerrorNoPlaceCar = (leng == "Esp") ? "por favor ingrese una placa valida" : ""
let alMesajeInfoSaveBuyComplete = (leng == "Esp") ?"Formulario coprado con exito" : ""
let SNumbersKey = "0123456789"
let SDecimalKey = "0123456789."
let VALIDATIONALFABETO = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLNMÑOPQUVWRSTXYZ "
let VALIDATIONALFANUMERICO = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLNMÑOPQRSTWUVXYZ1234567890 "

let FIREUrlFormsSoat = "FormsSoat"
let FIREUrlFormsSoatComplete = "FormsSoatComplete"
let ColorPing = UIColorFromRGB(rgbValue:0xFC4262)
let ColorErrorRed = UIColorFromRGB(rgbValue:0xFC4262)

var imageProfile:UIImage?
