//
//  GlobalFunc.swift
//  PruebaGrupoCMC
//
//  Created by Propio Data on 4/26/19.
//  Copyright © 2019 Propio Data. All rights reserved.
//

import Foundation
import UIKit
import Material

func ValidateStringContent(string:String, Exist:String) -> Bool
{
    
    if string == ""
    {
        return true
    }
    return Exist.contains(string)
}

func ValidationKeyNumbers(s:String, range:Int) -> Bool
{
    let aSet = NSCharacterSet(charactersIn: SNumbersKey).inverted
    //let aSet = NSCharacterSet(charactersInString:SNumbersKey).invertedSet
    let compSepByCharInSet = s.components(separatedBy: aSet)
    //let compSepByCharInSet = s.componentsSeparatedByCharactersInSet(aSet)
    let numberFiltered = compSepByCharInSet.joined(separator: "")
    
    
    return s == numberFiltered && s.count <= range
}


func ValidationEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let isemail = emailTest.evaluate(with: testStr)
    return isemail
}

func ChangeCurrencyAmount(tipAmount: Int) -> String
{
    let formatter = NumberFormatter()
    formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
    formatter.numberStyle = .currency
    if let formattedTipAmount = formatter.string(from: tipAmount as NSNumber) {
        return ": \(formattedTipAmount)"
    }
    else
    {
        return "$ 0"
    }
}
func CambioFormatoLetras (SCifra: String, MaxLong:String, MinLong:String) -> String
{
    
    print("\(Int64.max)")
    var Cifra:Int64 = 0
    let sNumerop = SCifra
    if sNumerop != ""
    {
        let num = sNumerop.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "'", with: "").replacingOccurrences(of: "$", with: "")
        //let num = sNumerop.stringByReplacingOccurrencesOfString(".", withString: "").stringByReplacingOccurrencesOfString("'", withString: "").stringByReplacingOccurrencesOfString("$", withString: "")
        if let Lnum = Int64(num)
        {
            Cifra = Lnum
            
            if Cifra > Int64(MaxLong)!
            {
                Cifra =  Int64(MaxLong)!
            }
            else if Cifra < Int64(MinLong)!
            {
                Cifra = Int64(MinLong)!
            }
        }
        else
        {
            return "0"
        }
    }
    else
    {
        return "0"
    }
    if Cifra <= 999
    {
        return "$\(Cifra)"
    }
    else if Cifra >= 1000 && Cifra <= 999999
    {
        let mil = Cifra / 1000
        let cen = Cifra - (mil * 1000)
        var scen = ""
        if "\(cen)".count <= 2
        {
            
            if "\(cen)".count == 1
            {
                scen = "00\(cen)"
            }
            else if "\(cen)".count == 2
            {
                scen = "0\(cen)"
            }
        }
        else
        {
            scen = "\(cen)"
        }
        return "$\(mil).\(scen)"
    }
    else if Cifra >= 1000000 && Cifra <= 999999999
    {
        let mill = Cifra / 1000000
        let mil = (Cifra - (mill * 1000000)) / 1000
        var smil = ""
        
        var scen = ""
        if "\(mil)".count <= 2
        {
            
            if "\(mil)".count == 1
            {
                smil = "00\(mil)"
            }
            else if "\(mil)".count == 2
            {
                smil = "0\(mil)"
            }
        }
        else
        {
            smil = "\(mil)"
        }
        let cen = Cifra - Int64("\(mill)\(smil)000")!
        if "\(cen)".count <= 2
        {
            
            if "\(cen)".count == 1
            {
                scen = "00\(cen)"
            }
            else if "\(cen)".count == 2
            {
                scen = "0\(cen)"
            }
        }
        else
        {
            scen = "\(cen)"
        }
        
        return "$\(mill).\(smil).\(scen)"
    }
    else
    {
        let milll = Cifra / 1000000000
        let mill = (Cifra - (milll * 1000000000)) / 1000000
        
        
        
        
        var smill = ""
        var smil = ""
        var scen = ""
        if "\(mill)".count <= 2
        {
            
            if "\(mill)".count == 1
            {
                smill = "00\(mill)"
            }
            else if "\(mill)".count == 2
            {
                smill = "0\(mill)"
            }
        }
        else
        {
            smill = "\(mill)"
        }
        let mil = (Cifra - Int64("\(milll)\(smill)000000")!) / 1000
        if "\(mil)".count <= 2
        {
            print("Contador chars milles: \("\(mil)".count)")
            
            if "\(mil)".count == 1
            {
                smil = "00\(mil)"
            }
            else if "\(mil)".count == 2
            {
                smil = "0\(mil)"
            }
            else
            {
                smil = "707"
            }
        }
        else
        {
            smil = "\(mil)"
        }
        
        let cen = Cifra - Int64("\(milll)\(smill)\(smil)000")!
        if "\(cen)".count <= 2
        {
            
            if "\(cen)".count == 1
            {
                scen = "00\(cen)"
            }
            else if "\(cen)".count == 2
            {
                scen = "0\(cen)"
            }
        }
        else
        {
            scen = "\(cen)"
        }
        
        
        return "$\(milll).\(smill).\(smil).\(scen)"
    }
    
    
}




func ValidationKeyDecimal(s:String, range:Int) -> Bool
{
    
    
    if s.components(separatedBy: ".").count >= 3
    {
        return false
    }
    let aSet = NSCharacterSet(charactersIn: SDecimalKey).inverted
    let compSepByCharInSet = s.components(separatedBy: aSet)
    let numberFiltered = compSepByCharInSet.joined(separator: "")
    
    
    return s == numberFiltered && s.count <= range
}
func ValidationKeyNumbersMax(s:String, range:Int, max: Int) -> Bool
{
    let aSet = NSCharacterSet(charactersIn: SNumbersKey).inverted
    let compSepByCharInSet = s.components(separatedBy: aSet)
    let numberFiltered = compSepByCharInSet.joined(separator: "")
    
    
    return s == numberFiltered && s.count <= range && Int(s)! <= max
}

func ValidationEmptyMandatoryField(viewc: UIView) -> Bool
{
    var permiso = true
    for vv in viewc.subviews as [UIView]
    {
        if vv is TextField
        {
            let textff = vv as! TextField
            if let ppholder = textff.placeholder
            {
                if textff.text == "" && ppholder.contains("*")
                {
                    if textff.isHidden == false
                    {
                        textff.detail = ErrorEmpty
                        permiso = false
                    }
                }
                
            }
            
            
        }
    }
    
    return permiso
}

class Colors {
    var gl:CAGradientLayer!
    
    init() {
        let colorTop = UIColor(red: 192.0 / 255.0, green: 38.0 / 255.0, blue: 42.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 35.0 / 255.0, green: 2.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0).cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
    }
}

func encryptMessage(message: String, encryptionKey: String) -> String {
    let messageData = message.data(using: .utf8)!
    //let cipherData = RNCryptor.encrypt(data: messageData, withPassword: encryptionKey)
    let base64 = messageData.base64EncodedString()
    return base64
}

func decryptMessage(encryptedMessage: String, encryptionKey: String) -> String {
    
    let encryptedData = Data.init(base64Encoded: encryptedMessage)!
    //let decryptedData = try RNCryptor.decrypt(data: encryptedData, withPassword: encryptionKey)
    let decryptedString = String(data: encryptedData, encoding: .utf8)!
    let base64 = decryptedString
    return base64
}



func getIFAddresses() -> String {
    var addresses = [String]()
    
    var address: String?
    var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
    if getifaddrs(&ifaddr) == 0 {
        var ptr = ifaddr
        while ptr != nil {
            defer { ptr = ptr?.pointee.ifa_next }
            
            let interface = ptr?.pointee
            let addrFamily = interface?.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
    }
    return (address == nil ) ? "10.0.0.1":address!
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}


extension String {
    
    // formatting text for currency textField
    func ofusquetStringDate(indextOf:Int, indextAt:Int) -> String {
        
        var Result = ""
        var i = 0
        for c in self{
            if (i >= indextOf && i <= indextAt){
                Result = Result + "*"
            }else{
                
                Result = Result + "\(c)"
            }
            i+=1
        }
        
        return Result
    }
}

enum CardType: String {
    case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
}
extension UITextField{
    
    func validateCreditCardFormat()-> (type: CardType, valid: Bool) {
        // Get only numbers from the input string
        var input = self.text!
        let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)//stringByReplacingOccurrencesOfString("[^0-9]", withString: "", options: .RegularExpressionSearch)
        
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false
        
        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }
        
        // check validity
        valid = luhnCheck(number: numberOnly)
        
        // format
        var formatted4 = ""
        for character in numberOnly {
            if formatted4.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(character)
        }
        
        formatted += formatted4 // the rest
        
        // return the tuple
        return (type, valid)
    }
    
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
    func luhnCheck(number: String) -> Bool {
        var sum = 0
        let digitStrings = number .reversed().map { String($0) }
        
        for tuple in digitStrings.enumerated() {
            guard let digit = Int(tuple.element) else { return false }
            let odd = tuple.offset % 2 == 1
            
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
}

func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func dateIsNMounthsBeforeDate(date1: Date, date2: Date, nMounts: Int) -> Bool {
    let calendar = Calendar.current

    let components = calendar.dateComponents([.month], from: date1, to: date2)
    
    return components.month! >= nMounts
}
func dateIsNDaysBeforeDate(date1: Date, date2: Date, nDays: Int) -> Bool {
    let calendar = Calendar.current
    
    let components = calendar.dateComponents([.day], from: date1, to: date2)
    
    return components.day! >= nDays
}
