//
//  FomDetailsViewController.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/27/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit

class FomrDetailsViewController: UIViewController {

    @IBOutlet weak var imProfile: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbDateExpired: UILabel!
    @IBOutlet weak var lbAdrress: UILabel!
    @IBOutlet weak var lbPlaceCar: UILabel!
    @IBOutlet weak var lbCity: UILabel!
    
    var FarmSoatSucces = FormModel(dic: ["":""])
    
    var presentor:FormDetailsProtocols?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detalle de compra"
        FormDetailRouter.createModule(controller: self)
        imProfile.image = imageProfile
        lbName.text = "Nombre: " + FarmSoatSucces.name + " " + FarmSoatSucces.lastName
        lbDateExpired.text = "Fecha de expiración: " + FarmSoatSucces.dateExpired
        lbAdrress.text = "Dirección: " + FarmSoatSucces.adrress
        lbPlaceCar.text = "Placa del carro: " + FarmSoatSucces.placeCar
        lbCity.text = "Ciudad: " + FarmSoatSucces.cityName
        // Do any additional setup af ter loading the view.
    }
    
    
    @IBAction func savePressed(_ sender: UIButton) {
        
        presentor?.saveForm(form: FarmSoatSucces)
    }

}

extension FomrDetailsViewController: FormDetailsPresenterToViewProtocol{
    func saveSucces(form:FormModel) {
        
        showAlertErrorComplete(View: self, Men: alMesajeInfoSaveBuyComplete, Complation: {
            
            self.performSegue(withIdentifier: "showFormSoat", sender: nil)
        })
        
    }
    
    func showError(error: String) {
        showAlertError(View: self, Men: error)
    }
    
    
}
