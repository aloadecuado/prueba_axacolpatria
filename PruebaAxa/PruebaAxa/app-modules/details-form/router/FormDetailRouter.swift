//
//  FormDetailRouter.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/28/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation

class FormDetailRouter : FormDetailsPresenterToRouterProtocol{
    static func createModule(controller: FomrDetailsViewController) {
        let presenter: FormDetailsProtocols & FormDetailsInteractorToPresenterProtocol = FormDetailsPresenter()
        let interactor: FormDetailsPresenterToInteractorProtocol = FormDetailsInteractor()
        let router:FormDetailsPresenterToRouterProtocol = FormDetailRouter()
        
        controller.presentor = presenter
        presenter.view = controller
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
    }
    
    
}
