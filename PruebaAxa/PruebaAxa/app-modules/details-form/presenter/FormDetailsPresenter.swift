//
//  FormDetailsPresenter.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/28/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
class FormDetailsPresenter: FormDetailsProtocols{
    var view: FormDetailsPresenterToViewProtocol?
    
    var interactor: FormDetailsPresenterToInteractorProtocol?
    
    var router: FormDetailsPresenterToRouterProtocol?
    
    func saveForm(form: FormModel) {
        interactor?.saveForm(form: form)
    }
    
    
}

extension FormDetailsPresenter:FormDetailsInteractorToPresenterProtocol{
    func saveSucces(form: FormModel) {
        view?.saveSucces(form: form)
    }
    
    func showError(error: String) {
        view?.showError(error: error)
    }
    

    


}
