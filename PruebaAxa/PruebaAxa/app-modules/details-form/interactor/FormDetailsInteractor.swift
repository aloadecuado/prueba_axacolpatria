//
//  FormDetailsInteractor.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/28/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation

class FormDetailsInteractor: FormDetailsPresenterToInteractorProtocol{
    var presenter: FormDetailsInteractorToPresenterProtocol?
    
    func saveForm(form: FormModel) {
        setObject(collection: FIREUrlFormsSoatComplete, dic: form.getDictionary(), ok: {res,key  in
            let formSoat = FormModel(dic:res)
            formSoat.uid = key
            self.presenter?.saveSucces(form: formSoat)
        }, error: {error in
            self.presenter?.showError(error:error)
        })
    }
    

    

    
    
}
