//
//  FormDetailsProtocols.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/28/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit
protocol FormDetailsProtocols: class{
    var view: FormDetailsPresenterToViewProtocol? {get set}
    var interactor: FormDetailsPresenterToInteractorProtocol? {get set}
    var router: FormDetailsPresenterToRouterProtocol? {get set}
    
    func saveForm(form:FormModel)
}

protocol FormDetailsPresenterToViewProtocol: class{
    func saveSucces(form:FormModel)
    func showError(error:String)
    
}

protocol FormDetailsPresenterToRouterProtocol: class {
    static func createModule(controller:FomrDetailsViewController)
}

protocol FormDetailsPresenterToInteractorProtocol: class {
    var presenter:FormDetailsInteractorToPresenterProtocol? {get set}
    func saveForm(form:FormModel)
}

protocol FormDetailsInteractorToPresenterProtocol: class {
    func saveSucces(form:FormModel)
    func showError(error:String)
}
