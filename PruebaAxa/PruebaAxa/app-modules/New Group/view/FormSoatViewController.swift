//
//  FormSoatViewController.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/26/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
class FormSoatViewController: UIViewController {

    var presentor:FormSoatPresentProtocol?
    
    @IBOutlet weak var svGeneral: UIScrollView!
    @IBOutlet weak var imProfile: UIImageView!
    @IBOutlet weak var btImageProfile: UIButton!
    @IBOutlet weak var etName: IndetTextField!
    @IBOutlet weak var etLastName: IndetTextField!
    @IBOutlet weak var etNumberId: IndetTextField!
    @IBOutlet weak var etExpiredDate: IndetTextField!
    @IBOutlet weak var etPlaceCar: IndetTextField!
    @IBOutlet weak var etCity: IndetTextField!
    @IBOutlet weak var etAddress: IndetTextField!
    @IBOutlet weak var pvCitys: UIPickerView!
    
    var citysColombia = [CitysColombiaModel]()
    var selectPickerCity = CitysColombiaModel(dic: ["":""])
    
    var imagePicker: UIImagePickerController!
    
    var FarmSoatSucces = FormModel(dic: ["":""])
    
    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    let locationManager = CLLocationManager()
    
    var location = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        FormSoatRouter.createModule(controller: self)
        self.title = "Formulario"
        
        etName.setTextoAlfabetico(Rango: 20)
        etLastName.setTextoAlfabetico(Rango: 20)
        etNumberId.setTextoNumerico(Rango: 12)
        etExpiredDate.setTextoFecha(Controller: self)
        etPlaceCar.setTexto(Rango: 6)
        pvCitys.delegate = self
        etCity.delegate = self
        etAddress.delegate = self
        svGeneral.contentSize.height = 1000.0
        presentor?.startFetchingCitysColombia()
        setLocationsPermissions()
        

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FormSoatViewController.dismissKeyboard))
        
        
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    func setLocationsPermissions(){
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    @IBAction func saveFormSoatPressed(_ sender: UIBarButtonItem) {
        let form = FormModel(dic: ["":""])
        form.name = etName.text!
        form.lastName = etLastName.text!
        form.numberId = etNumberId.text!
        form.dateExpired = etExpiredDate.text!
        form.cityName = selectPickerCity.name
        form.cityId = selectPickerCity.id
        form.placeCar = etPlaceCar.text!
        form.adrress = etAddress.text!
        imageProfile = imProfile.image
        presentor?.saveForm(view: svGeneral, imagePhoto: imProfile, form: form)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"{
            let vc = segue.destination as! FomrDetailsViewController
            vc.FarmSoatSucces = FarmSoatSucces
        }
    }
}
extension FormSoatViewController:PresenterToViewProtocol{
    
    func fetchCitysColombia(citys: [CitysColombiaModel]) {
        citysColombia = citys
        pvCitys.reloadAllComponents()
    }
    
    func formSave(form: FormModel) {
        FarmSoatSucces = form
        
        self.performSegue(withIdentifier: "showDetail", sender: nil)
    }
    
    func showError(error:String) {
        showAlertError(View: self, Men: error)
    }
    
    
}

extension FormSoatViewController:UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return citysColombia.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return citysColombia[row].name
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pvCitys.isHidden = true;
        etCity.text = citysColombia[row].name
        selectPickerCity.name = citysColombia[row].name
        selectPickerCity.id = citysColombia[row].id
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == etCity{
            self.pvCitys.isHidden = false;
            textField.resignFirstResponder()
            view.endEditing(true)
        }else{
            textField.resignFirstResponder()
            view.endEditing(true)
            presentor?.PresentModalUbicacion(view: self, location: location, Aceptar: {adrress in
                
                self.etAddress.text = adrress
            })
        }
        
    }
    
}

extension FormSoatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBAction func selectImagePressed(_ sender: UIButton) {
        
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
    
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        btImageProfile.setBackgroundImage(nil, for: .normal)
        imProfile.image = selectedImage
    }
}

extension FormSoatViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        location = locValue
    }
}




