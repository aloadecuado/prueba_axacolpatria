//
//  FormSoatPresenter.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/27/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit
import FirebaseUI
import CoreLocation
class FormSoatPresenter:FormSoatPresentProtocol{

    
    
    
    var view: PresenterToViewProtocol?
    
    var interactor: PresenterToInteractorProtocol?
    
    var router: PresenterToRouterProtocol?
    func saveForm(view: UIView, imagePhoto:UIImageView, form: FormModel) {
        interactor?.saveForm(view: view, imagePhoto: imagePhoto, form: form)
    }
    func startFetchingCitysColombia() {
        interactor?.fetchCitysColombia();
    }
    
    func showDetailsForm(navigationController: UINavigationController) {
        
    }
    
    func PresentModalUbicacion(view: UIViewController, location: CLLocationCoordinate2D, Aceptar: @escaping ((String) -> Void)) {
        router?.PresentModalUbicacion(view: view, location: location, Aceptar: Aceptar)
    }
}

extension FormSoatPresenter:InteractorToPresenterProtocol{
    
    func showError(error:String) {
        view?.showError(error: error)
    }
    
    func fetchCitysColombia(citys: [CitysColombiaModel]) {
        view?.fetchCitysColombia(citys: citys);
    }
    
    func formSave(form: FormModel) {
        view?.formSave(form: form)
    }
    
    
    
}
