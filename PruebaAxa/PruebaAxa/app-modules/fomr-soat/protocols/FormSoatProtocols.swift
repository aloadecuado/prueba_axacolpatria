//
//  FormSoatProtocols.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/26/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit
import FirebaseUI
import CoreLocation
protocol FormSoatPresentProtocol: class{
    var view: PresenterToViewProtocol? {get set}
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    
    func saveForm(view:UIView, imagePhoto:UIImageView, form:FormModel)
    func startFetchingCitysColombia()
    func PresentModalUbicacion(view:UIViewController, location:CLLocationCoordinate2D, Aceptar: @escaping ((String) -> Void))
}

protocol PresenterToViewProtocol: class{
    func formSave(form:FormModel)
    func showError(error:String)
    func fetchCitysColombia(citys:[CitysColombiaModel])

}

protocol PresenterToRouterProtocol: class {
    static func createModule(controller:FormSoatViewController)
    func PresentModalUbicacion(view:UIViewController, location:CLLocationCoordinate2D, Aceptar: @escaping ((String) -> Void))
}

protocol PresenterToInteractorProtocol: class {
    var presenter:InteractorToPresenterProtocol? {get set}
    func saveForm(view:UIView, imagePhoto:UIImageView,form:FormModel)
    func fetchCitysColombia()
}

protocol InteractorToPresenterProtocol: class {
    func formSave(form:FormModel)
    func showError(error:String)
    func fetchCitysColombia(citys:[CitysColombiaModel])
}
