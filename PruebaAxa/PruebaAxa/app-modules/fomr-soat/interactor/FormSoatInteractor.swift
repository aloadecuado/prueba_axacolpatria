//
//  FormSoatInteractor.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/27/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import Firebase
import FirebaseUI

class FormSoatInteractor:PresenterToInteractorProtocol {


    
    var presenter: InteractorToPresenterProtocol?

    
    
    func saveForm(view: UIView, imagePhoto:UIImageView, form: FormModel) {
        
        if (ValidationEmptyMandatoryField(viewc: view)){
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let date = formatter.date(from:form.dateExpired)!
            if(dateIsNMounthsBeforeDate(date1: Date(), date2: date, nMounts: 3)){
                
                presenter?.showError(error: alMessageerrorBeforeDate)
                return
            }
            if(!dateIsNDaysBeforeDate(date1: Date(), date2: date, nDays: 0)){
                
                presenter?.showError(error: alMessageerrorBeforeDate)
                return
            }
            
            if (imagePhoto.image == nil){
                presenter?.showError(error: alMessageerrorNoPhotto)
                return
            }
            
            if(isPlaceCarValid(testStr: form.placeCar)){
                presenter?.showError(error: alMessageerrorNoPlaceCar)
                return
            }
            setObject(collection: FIREUrlFormsSoat, dic: form.getDictionary(), ok: {res,key  in
                let formSoat = FormModel(dic:res)
                formSoat.uid = key
                self.presenter?.formSave(form: formSoat)
            }, error: {error in
                self.presenter?.showError(error:error)
            })
            
            
        }
    }
    func isPlaceCarValid(testStr:String) -> Bool {
        let emailRegEx = "[A-Z]{1,3}-[A-Z]{1,2}-[0-9]{1,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    
    func fetchCitysColombia() {
        let data = RemoteConfig.remoteConfig().configValue(forKey: "citys_colombia").dataValue
        
        var array:NSArray?
        
        do {
            array = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray
            // use anyObj here
            
            var citysArray = [CitysColombiaModel]()
            for dic in array!{
                let city = CitysColombiaModel(dic: dic as! NSDictionary)
                citysArray.append(city)
            }
            presenter?.fetchCitysColombia(citys: citysArray)
        } catch {
            
            print("json error: \(error)")
            presenter?.showError(error: error.localizedDescription)
            return
        }
    }

}


