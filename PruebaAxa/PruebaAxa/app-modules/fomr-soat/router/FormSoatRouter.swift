//
//  FormSoatRouter.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/27/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
class FormSoatRouter:PresenterToRouterProtocol{
    
    
    static func createModule(controller:FormSoatViewController)  {
        //let view = mainstoryboard.instantiateViewController(withIdentifier: "FormSoatViewController") as! FormSoatViewController
        
        let presenter: FormSoatPresentProtocol & InteractorToPresenterProtocol = FormSoatPresenter()
        let interactor: PresenterToInteractorProtocol = FormSoatInteractor()
        let router:PresenterToRouterProtocol = FormSoatRouter()
        
        controller.presentor = presenter
        presenter.view = controller
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
    }
    
    
    func PresentModalUbicacion(view:UIViewController, location:CLLocationCoordinate2D, Aceptar: @escaping ((String) -> Void))
    {
        
        let viewController = FormSoatRouter.mainstoryboard.instantiateViewController(withIdentifier: "PopUpUbicarViewController") as! PopUpUbicarViewController
        viewController.location = location
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        
        viewController.AcptarUbicacion = {rest in
            
            UserDefaults.standard.set(rest, forKey: "sValueKeyubicaicon")
            Aceptar(rest)
            //SetDataDataImages(anexos: self.ArrayImageAnexos, documentos: self.ArrayImageDocuments)
            
        }
        //self.navigationController?.pushViewController(viewController, animated: true)
        view.present(viewController, animated: true, completion: nil)
    }
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    
}
