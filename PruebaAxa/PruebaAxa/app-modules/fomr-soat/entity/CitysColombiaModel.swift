//
//  CitysColombiaModel.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/27/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit

class CitysColombiaModel:NSObject{
     var id = 0;
     var name  = "";
    
    init(dic:NSDictionary){
        id = ValueJsonInt(dic: dic, key: "id")
        name = ValueJsonString(dic: dic, key: "name")
    }
}
