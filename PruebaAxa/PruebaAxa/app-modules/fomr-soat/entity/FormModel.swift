//
//  FormModel.swift
//  PruebaAxa
//
//  Created by Propio Data on 5/27/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit

class FormModel: NSObject {
    
    var name = ""
    var lastName = ""
    var numberId = ""
    var dateExpired = ""
    var placeCar = ""
    var cityName = ""
    var cityId = 0
    var adrress = ""
    var uid = ""
    
    func getDictionary() -> NSDictionary{
        return ["name":name, "lastName":lastName, "numberId":numberId, "dateExpired":dateExpired, "placeCar":placeCar, "cityName":cityName, "cityId":cityId, "adrress":adrress, "uid":uid]
    }
    
    init(dic:NSDictionary){
        name = ValueJsonString(dic: dic, key: "name")
        lastName = ValueJsonString(dic: dic, key: "lastName")
        numberId = ValueJsonString(dic: dic, key: "numberId")
        dateExpired = ValueJsonString(dic: dic, key: "dateExpired")
        placeCar = ValueJsonString(dic: dic, key: "placeCar")
        cityName = ValueJsonString(dic: dic, key: "cityName")
        cityId = ValueJsonInt(dic: dic, key: "cityId")
        uid = ValueJsonString(dic: dic, key: "uid")
        adrress = ValueJsonString(dic: dic, key: "adrress")
    }

}
